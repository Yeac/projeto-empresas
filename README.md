![N|Solid](logo_ioasys.png)

# README #

## Projecto-empresas ##

In this repository I developed an application following the steps and layouts by ioasys.
Some project advices:
* IDE - Android Studio 3.4.1
* Compiler - Gradle 5.1.1
* Language - Java
* Device Tested - Android N (API 24)

### Libraries ###
* Butterknife 10.1.0
	* Free library to inject view in Android components.
	* It helps to reduce the amount of coding.
	* Easily binds components from views and resources to code.
	* I used it for binding views to code.

* Retrofit 2.6.0
	* Free Library for HTTP request for connecting web services.
	* Asynchronus execution for web services requests.
	* Less coding for services communication.
	* Allows easily REST client and server communication.
	* Has different converters based on data response.
	* Allows OAuth 2.0 configuration.
	* I used it for get the web services connection and oauth2 headers.

* Retrofit Gson Converter 2.6.0
	* Converter for JSON request/response while using retrofit client.
	* Used for getting REST web service response.
	* Used for parsing objects to json, and vice versa.

* Picasso 2.71828
	* Free library for image processing.
	* Easily, sets and URL image to ImageView component.
	* It helped me to upload the url images from ioasys services to the app.

### If I had more time ###
* Material design is a challenge for me, could have done more research for this project.
* Validations for internet connection, web service error responses and for crashing.
* Validation for modern devices, not sure how it is going to answer with Android P (API 28) devices.
* Use in-app database.
* Use kotlin.
* Screen based views.
* Testing environment.
* Create a module for specific search, services could be used as ID or ENTERPRISENAME search.

### How to execute application ###
* At first, you will need Android 3.4.1 IDE with Gradle 5.1.1 compiler.
* Be sure to use an internet conection without proxy.
* First, go to Gradle sync option and press it.
	* If it doesn't work and throws this error:
			Failed to open zip file.
			Gradle's dependency cache may be corrupt (this sometimes occurs after a network connection timeout.)
	* Try Downloading Gradle 5.1.1 from: https://gradle.org/releases/
	* Then, in the android project.
		* Go to: File\Settings\Build,Execution,Deployment\Gradle
		* Select the "Use local Gradle distribution" option.
		* You will have to make the reference to that downloaded file.
		* apply and sync again.
* Don't update compiler while Gradle is synchronizing.
* After that, go to Build\Clean Project, wait till it ends.
* Then go to Build\Rebuild Project, wait till it ends.
* We will have two options here:
	* Build apk
		* Then go to Build\Bundle(s)/APK(s)\Build APK(s), wait till it ends.
		* The project will generate a APK and you will be able to install it in Android Devices.
	* Execute in emulator / device
		* Create a Android N (API 24) emulator or connect the device.
		* Go to the button Run and configure the project to execute (this should have been done automatically).
		* Select the device where to execute.
		* Wait till the app opens.
* As I said before, try testing on Android N (API 24) devices.

-- Thank you for your time, have a nice week!
- Miguel Yengle


# ioasys - README #

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto Empresas.

### O QUE FAZER ? ###

* Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso.

### ESCOPO DO PROJETO ###

* Deve ser criado um aplicativo Android utilizando linguagem Java ou Kotlin com as seguintes especificações:
* Login e acesso de Usuário já registrado
	* Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);
	* Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;
* Listagem de Empresas
* Detalhamento de Empresas

### Informações Importantes ###

* Layout e recortes disponíveis no Zeplin (http://zeplin.io)
Login - teste_ioasys
Senha - ioasys123

* Integração disponível a partir de uma collection para Postman (https://www.getpostman.com/apps) disponível neste repositório.
* O `README.md` deve conter uma pequena justificativa de cada biblioteca adicionada ao projeto como dependência.
* O `README.md` deve conter tambem o que você faria se tivesse mais tempo.
* O `README.md` do projeto deve conter instruções de como executar a aplicação
* Independente de onde conseguiu chegar no teste é importante disponibilizar seu fonte para analisarmos.

### Dados para Teste ###

* Servidor: http://empresas.ioasys.com.br
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

### Dicas ###

* Para requisição sugerimos usar a biblioteca Retrofit
* Para download e cache de imagens use a biblioteca Glide
* Para parse de Json use a biblioteca GSON

### Bônus ###

* Testes unitários, pode usar a ferramenta que você tem mais experiência, só nos explique o que ele tem de bom.
* Usar uma arquitetura testável. Ex: MVP, MVVM, Clean, etc.
* Material Design
* Utilizar alguma ferramenta de Injeção de Dependência, Dagger, Koin e etc..
* Utilizar Rx, LiveData, Coroutines.
* Padrões de projetos
