package com.projeto.empresas.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.projeto.empresas.R;
import com.projeto.empresas.activities.DetailEmpresaActivity;
import com.projeto.empresas.models.MOEmpresa;
import com.projeto.empresas.models.MOEnterprise;
import com.projeto.empresas.util.EmpresasConstants;
import com.projeto.empresas.util.EmpresasParser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class EmpresasAdapter extends RecyclerView.Adapter<EmpresasViewHolder> {

    private List<MOEnterprise> listEnterprise;
    private Context context;

    public EmpresasAdapter(List<MOEnterprise> plist, Context pcontext) {
        this.listEnterprise = plist;
        this.context = pcontext;
    }

    @NonNull
    @Override
    public EmpresasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_empresa, parent, false);
        EmpresasViewHolder holder = new EmpresasViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull EmpresasViewHolder holder, int position) {
        MOEnterprise obj = listEnterprise.get(position);

        if (obj.getPhoto() != null && !obj.getPhoto().isEmpty()) {
            Picasso.get()
                    .load(EmpresasConstants.PHOTO_URL + obj.getPhoto())
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_foreground)
                    .fit()
                    .noFade()
                    .into(holder.tvImageView);
        } else {
            holder.tvImageView.setImageResource(R.drawable.ic_launcher_background);
        }

        holder.tvEmpresa.setText(obj.getEnterpriseName());

        if (obj.getEnterpriseType() != null)
            holder.tvNegocio.setText(obj.getEnterpriseType().getEnterpriseTypeName());
        else
            holder.tvNegocio.setText(R.string.list_empresa_undefined);

        holder.tvPais.setText(obj.getCountry());

        holder.itemView.setOnClickListener(v -> {
            String json = EmpresasParser.parseEmpresaJson(obj);
            Intent intent = new Intent(context, DetailEmpresaActivity.class);
            intent.putExtra("objenterprise", json);
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listEnterprise.size();
    }

    public void updateList(List<MOEnterprise> plist) {
        this.listEnterprise = new ArrayList<>();
        this.listEnterprise = plist;
        notifyDataSetChanged();
    }
}
