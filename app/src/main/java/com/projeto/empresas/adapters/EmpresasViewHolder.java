package com.projeto.empresas.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.projeto.empresas.R;

public class EmpresasViewHolder extends RecyclerView.ViewHolder {

    public ImageView tvImageView;
    public TextView tvEmpresa;
    public TextView tvNegocio;
    public TextView tvPais;

    public EmpresasViewHolder(@NonNull View itemView) {
        super(itemView);

        tvImageView = itemView.findViewById(R.id.tv_list_img);
        tvEmpresa = itemView.findViewById(R.id.tv_list_empresa);
        tvNegocio = itemView.findViewById(R.id.tv_list_negocio);
        tvPais = itemView.findViewById(R.id.tv_list_pais);

    }
}
