package com.projeto.empresas.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projeto.empresas.R;
import com.projeto.empresas.adapters.EmpresasAdapter;
import com.projeto.empresas.models.MOAuthCredentials;
import com.projeto.empresas.models.MOEmpresa;
import com.projeto.empresas.models.MOEnterprise;
import com.projeto.empresas.network.ApiClient;
import com.projeto.empresas.network.ApiInterface;
import com.projeto.empresas.network.response.RPFindAll;
import com.projeto.empresas.util.EmpresasSharedPreferences;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.tb_searchtoolbar)
    Toolbar searchToolbar;
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.rv_empresas)
    RecyclerView rvEmpresas;

    private LinearLayoutManager linearLayoutManager;
    private EmpresasAdapter empresasAdapter;
    private DialogInterface.OnClickListener dialogListener =
            (dialogInterface, answer) -> {
                switch (answer) {
                    case DialogInterface.BUTTON_POSITIVE:
                        signOut();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            };

    private List<MOEnterprise> listEnterprise;
    private List<MOEnterprise> listFilter;
    private ApiInterface mApiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ButterKnife.bind(this);
        setSupportActionBar(searchToolbar);

        listEnterprise = new ArrayList<>();
        findAllEnterprises();

        linearLayoutManager = new LinearLayoutManager(this);
        rvEmpresas.setLayoutManager(linearLayoutManager);
        rvEmpresas.setHasFixedSize(true);
        empresasAdapter = new EmpresasAdapter(new ArrayList<>(), this);
        rvEmpresas.setAdapter(empresasAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        MenuItem.OnActionExpandListener actionExpand = new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                tvSearch.setVisibility(View.GONE);
                rvEmpresas.setVisibility(View.VISIBLE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                tvSearch.setVisibility(View.VISIBLE);
                rvEmpresas.setVisibility(View.GONE);
                return true;
            }
        };

        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        searchItem.setOnActionExpandListener(actionExpand);

        return true;
    }

    @Override
    public void onBackPressed() {
        confirmLogout();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (!newText.isEmpty()) {
            String enterprisename, filter;
            List<MOEnterprise> listFilter = new ArrayList<>();
            for (MOEnterprise x : listEnterprise) {
                enterprisename = x.getEnterpriseName().toLowerCase();
                filter = newText.toLowerCase();
                if (enterprisename.contains(filter)) {
                    listFilter.add(x);
                }
            }
            empresasAdapter.updateList(listFilter);
        } else {
            empresasAdapter.updateList(listEnterprise);
        }
        return true;
    }

    public void confirmLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this);
        builder.setTitle(getString(R.string.search_box_question))
                .setPositiveButton(getString(R.string.search_box_yes), dialogListener)
                .setNegativeButton(getString(R.string.search_box_no), dialogListener)
                .show();
    }

    public void signOut() {
        EmpresasSharedPreferences.resetPreferences(this);
        finish();
    }

    public void findAllEnterprises() {
        MOAuthCredentials auth = EmpresasSharedPreferences.getAuthCredentialsSp(this);
        mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<RPFindAll> call = mApiInterface.getAllEnterprises(
                auth.getAccess_token(),
                auth.getClient(),
                auth.getUid());
        call.enqueue(new Callback<RPFindAll>() {
            @Override
            public void onResponse(Call<RPFindAll> call, Response<RPFindAll> response) {
                if (response.isSuccessful()) {
                    if (response.body().getEnterprises() != null) {
                        if (!response.body().getEnterprises().isEmpty()) {
                            listEnterprise= response.body().getEnterprises();
                            empresasAdapter.updateList(listEnterprise);
                        }
                    } else {
                        Toast.makeText(SearchActivity.this, "List is empty", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SearchActivity.this, "Error on getting list", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RPFindAll> call, Throwable t) {
                Toast.makeText(SearchActivity.this, "Error network", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //didnt used the services with filters, but it could look like this
    public void findByFilter(String filter) {
        MOAuthCredentials auth = EmpresasSharedPreferences.getAuthCredentialsSp(this);
        mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<RPFindAll> call = mApiInterface.getEnterprisesFilter(
                auth.getAccess_token(),
                auth.getClient(),
                auth.getUid(),
                filter);
        call.enqueue(new Callback<RPFindAll>() {
            @Override
            public void onResponse(Call<RPFindAll> call, Response<RPFindAll> response) {
                if (response.isSuccessful()) {
                    if (!response.body().getEnterprises().isEmpty()) {
                        List<MOEnterprise> list = response.body().getEnterprises();
                        empresasAdapter.updateList(list);
                    } else {
                        Toast.makeText(SearchActivity.this, "List is empty", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SearchActivity.this, "Error on getting list", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RPFindAll> call, Throwable t) {
                Toast.makeText(SearchActivity.this, "Error network", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
