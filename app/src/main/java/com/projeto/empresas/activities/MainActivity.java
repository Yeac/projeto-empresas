package com.projeto.empresas.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.projeto.empresas.R;
import com.projeto.empresas.network.ApiClient;
import com.projeto.empresas.network.ApiInterface;
import com.projeto.empresas.network.util.ApiUtil;
import com.projeto.empresas.network.request.RQLoginCred;
import com.projeto.empresas.network.response.RPLoginCred;
import com.projeto.empresas.network.response.RPLoginCredError;
import com.projeto.empresas.util.EmpresasStrings;
import com.projeto.empresas.util.EmpresasUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btn_entrar)
    Button btnEntrar;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;

    private ApiInterface mApiInterface;
    private ProgressDialog mProgressLoggin;
    private AlertDialog.Builder mDialogLoggin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EmpresasUtil.fullScreen(this);
        ButterKnife.bind(this);

        etEmail.setText(EmpresasStrings.email);
        etPassword.setText(EmpresasStrings.passw);
        btnEntrar.setOnClickListener(v -> {
            startProgress(getString(R.string.login_progress_message), getString(R.string.login_progress_title), this);
            String email = etEmail.getText().toString();
            String passw = etPassword.getText().toString();
            if (inputIsValid()) {
                loginService(email, passw);
            } else {
                mProgressLoggin.hide();
                if (email.isEmpty() || EmpresasUtil.ValidateEmail(email)) {
                    etEmail.setError(getApplicationContext().getString(R.string.login_error_email));
                }
                if (passw.isEmpty()) {
                    etPassword.setError(getApplicationContext().getString(R.string.login_error_passw));
                }
            }
        });
    }

    private boolean inputIsValid() {
        String email = etEmail.getText().toString();
        String passw = etPassword.getText().toString();
        if (EmpresasUtil.ValidateEmail(email)) return false;
        if (EmpresasUtil.IsEmptyOrNull(email)) return false;
        if (EmpresasUtil.IsEmptyOrNull(passw)) return false;
        return true;
    }

    private void loginService(String email, String passw) {
        EmpresasUtil.hideKeyboard(this);
        RQLoginCred request = new RQLoginCred();
        request.setEmail(email);
        request.setPassword(passw);
        mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<RPLoginCred> call = mApiInterface.getUserCredentials(request);
        call.enqueue(new Callback<RPLoginCred>() {
            @Override
            public void onResponse(Call<RPLoginCred> call, Response<RPLoginCred> response) {
                if (response.isSuccessful()) {
                    ApiUtil.setAuthCredentials(response, getApplicationContext());
                    ApiUtil.setUserProfile(response.body(), getApplicationContext());
                    mProgressLoggin.hide();
                    startActivity(new Intent(MainActivity.this, SearchActivity.class));
                } else {
                    RPLoginCredError error = ApiUtil.ApiErrorParse(response);
                    String errorMessage = ApiUtil.getApiErrorMessage(error);
                    mProgressLoggin.hide();
                    showMessageBox(errorMessage,
                            getApplicationContext().getString(R.string.login_box_title),
                            MainActivity.this);
                }
            }

            @Override
            public void onFailure(Call<RPLoginCred> call, Throwable t) {
                Toast.makeText(MainActivity.this, getApplicationContext().getString(R.string.login_message_error), Toast.LENGTH_SHORT).show();
                mProgressLoggin.hide();
            }
        });
    }

    public void startProgress(String message, String title, Context context) {
        mProgressLoggin = new ProgressDialog(context);
        mProgressLoggin.setMessage(message);
        mProgressLoggin.setTitle(title);
        mProgressLoggin.setIndeterminate(false);
        mProgressLoggin.setCancelable(false);
        mProgressLoggin.show();
    }

    public void showMessageBox(String message, String title, Context context) {
        mDialogLoggin = new AlertDialog.Builder(context);
        mDialogLoggin.setMessage(message);
        mDialogLoggin.setTitle(title);
        mDialogLoggin.setPositiveButton(context.getString(R.string.login_box_ok), (dialog, which) -> {
        });
        mDialogLoggin.setCancelable(true);
        mDialogLoggin.create().show();
    }

}
