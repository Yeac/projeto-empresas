package com.projeto.empresas.activities;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.projeto.empresas.R;
import com.projeto.empresas.models.MOEnterprise;
import com.projeto.empresas.util.EmpresasConstants;
import com.projeto.empresas.util.EmpresasParser;
import com.projeto.empresas.util.EmpresasSharedPreferences;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailEmpresaActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar detailToolbar;
    @BindView(R.id.tv_detail_img)
    ImageView ivDetailImg;
    @BindView(R.id.tv_detail_desc)
    TextView tvDetailDesc;

    private MOEnterprise moEnterprise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_empresa);

        ButterKnife.bind(this);

        String json = getIntent().getStringExtra("objenterprise");
        moEnterprise = EmpresasParser.parseJsonEmpresa(json);

        setSupportActionBar(detailToolbar);
        getSupportActionBar().setTitle(moEnterprise != null ? moEnterprise.getEnterpriseName() : "error");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvDetailDesc.setText(moEnterprise != null ? moEnterprise.getDescription() : "error");

        if (moEnterprise.getPhoto() != null && !moEnterprise.getPhoto().isEmpty()) {
            Picasso.get()
                    .load(EmpresasConstants.PHOTO_URL + moEnterprise.getPhoto())
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_foreground)
                    .fit()
                    .noFade()
                    .into(ivDetailImg);
        } else {
            ivDetailImg.setImageResource(R.drawable.ic_launcher_background);
        }

    }

}
