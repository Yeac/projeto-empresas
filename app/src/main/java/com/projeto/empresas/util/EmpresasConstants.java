package com.projeto.empresas.util;

public class EmpresasConstants {

    public static final String ARG_CONTENT_TYPE = "application/json";
    public static final String BASE_URL = "http://empresas.ioasys.com.br/";
    public static final String PHOTO_URL = "http://empresas.ioasys.com.br";

}
