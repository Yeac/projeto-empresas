package com.projeto.empresas.util;

import android.content.Context;

import com.projeto.empresas.models.MOAuthCredentials;
import com.projeto.empresas.models.MOProfile;

public class EmpresasSharedPreferences {

    public static void resetPreferences(Context context) {
        context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
    }

    public static void setAccessTokenSp(Context context, String token) {
        context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .edit()
                .putString(EmpresasStrings.ARG_TOKEN, token)
                .apply();
    }

    public static String getAccesTokenSp(Context context) {
        return context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getString(EmpresasStrings.ARG_TOKEN, null);
    }

    public static void setClientSp(Context context, String client) {
        context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .edit()
                .putString(EmpresasStrings.ARG_CLIENT, client)
                .apply();
    }

    public static String getClientSp(Context context) {
        return context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getString(EmpresasStrings.ARG_CLIENT, null);
    }

    public static void setUidSp(Context context, String uid) {
        context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .edit()
                .putString(EmpresasStrings.ARG_UID, uid)
                .apply();
    }

    public static String getUidSp(Context context) {
        return context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getString(EmpresasStrings.ARG_UID, null);
    }

    public static void setAuthCredentialsSp(Context context, MOAuthCredentials auth) {
        String json = EmpresasParser.parseAuthCredentialsJson(auth);
        context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .edit()
                .putString(EmpresasStrings.ARG_AUTH, json)
                .apply();
    }

    public static MOAuthCredentials getAuthCredentialsSp(Context context) {
        String json = context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getString(EmpresasStrings.ARG_AUTH, null);
        return EmpresasParser.parseJsonAuthCredentials(json);
    }

    public static void setProfileSp(Context context, MOProfile auth) {
        String json = EmpresasParser.parseProfileJson(auth);
        context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .edit()
                .putString(EmpresasStrings.ARG_PROFILE, json)
                .apply();
    }

    public static MOProfile getProfileSp(Context context) {
        String json = context.getSharedPreferences(EmpresasStrings.EMPRESAS_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getString(EmpresasStrings.ARG_PROFILE, null);
        return EmpresasParser.parseJsonProfile(json);
    }

}
