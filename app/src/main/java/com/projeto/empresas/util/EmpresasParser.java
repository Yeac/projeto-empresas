package com.projeto.empresas.util;

import com.google.gson.Gson;
import com.projeto.empresas.models.MOAuthCredentials;
import com.projeto.empresas.models.MOEmpresa;
import com.projeto.empresas.models.MOEnterprise;
import com.projeto.empresas.models.MOProfile;

public class EmpresasParser {

    public static String parseAuthCredentialsJson(MOAuthCredentials obj) {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        return json;
    }

    public static MOAuthCredentials parseJsonAuthCredentials(String json) {
        if (json == null || json.isEmpty())
            return null;
        Gson gson = new Gson();
        MOAuthCredentials send = gson.fromJson(json, MOAuthCredentials.class);
        return send;
    }

    public static String parseProfileJson(MOProfile obj) {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        return json;
    }

    public static MOProfile parseJsonProfile(String json) {
        if (json == null || json.isEmpty())
            return null;
        Gson gson = new Gson();
        MOProfile send = gson.fromJson(json, MOProfile.class);
        return send;
    }

    public static String parseEmpresaJson(MOEnterprise obj) {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        return json;
    }

    public static MOEnterprise parseJsonEmpresa(String json) {
        if (json == null || json.isEmpty())
            return null;
        Gson gson = new Gson();
        MOEnterprise send = gson.fromJson(json, MOEnterprise.class);
        return send;
    }
}
