package com.projeto.empresas.util;

public class EmpresasStrings {

    public static final String EMPRESAS_SHARED_PREFERENCES = "projeto.empresas.sp";

    public static final String ARG_TOKEN = "empresas.token";
    public static final String ARG_CLIENT = "empresas.client";
    public static final String ARG_UID = "empresas.uid";
    public static final String ARG_AUTH = "empresas.auth";
    public static final String ARG_PROFILE = "empresas.profile";

    public static final String EMAIL_PATTERN = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

    //for testing
    public static final String email = "testeapple@ioasys.com.br";
    public static final String passw = "12341234";


}
