package com.projeto.empresas.network.response;

import com.google.gson.annotations.SerializedName;
import com.projeto.empresas.models.MOInvestor;

import java.io.Serializable;
import java.util.List;

public class RPLoginCredError implements Serializable {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("errors")
    private List<String> errors = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
