package com.projeto.empresas.network.response;

import com.google.gson.annotations.SerializedName;
import com.projeto.empresas.models.MOEnterprise;

import java.io.Serializable;
import java.util.List;

public class RPFindAll implements Serializable {

    @SerializedName("enterprises")
    private List<MOEnterprise> enterprises = null;

    public List<MOEnterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<MOEnterprise> enterprises) {
        this.enterprises = enterprises;
    }
}
