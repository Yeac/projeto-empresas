package com.projeto.empresas.network;

import com.projeto.empresas.network.request.RQLoginCred;
import com.projeto.empresas.network.response.RPFindAll;
import com.projeto.empresas.network.response.RPLoginCred;
import com.projeto.empresas.util.EmpresasConstants;
import com.projeto.empresas.util.EmpresasSharedPreferences;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @Headers({"Content-Type: " + EmpresasConstants.ARG_CONTENT_TYPE})
    @POST("api/v1/users/auth/sign_in")
    Call<RPLoginCred> getUserCredentials(@Body RQLoginCred login);

    @Headers({"Content-Type: " + EmpresasConstants.ARG_CONTENT_TYPE,})
    @GET("api/v1/enterprises")
    Call<RPFindAll> getAllEnterprises(
            @Header("access-token") String auth,
            @Header("client") String client,
            @Header("uid") String uid);

    @Headers({"Content-Type: " + EmpresasConstants.ARG_CONTENT_TYPE,})
    @GET("api/v1/enterprises")
    Call<RPFindAll> getEnterprisesFilter(
            @Header("access-token") String auth,
            @Header("client") String client,
            @Header("uid") String uid,
            @Query("name") String name);

}
