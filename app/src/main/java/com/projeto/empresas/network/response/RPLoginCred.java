package com.projeto.empresas.network.response;

import com.google.gson.annotations.SerializedName;
import com.projeto.empresas.models.MOInvestor;

import java.io.Serializable;
import java.util.List;

public class RPLoginCred implements Serializable {

    @SerializedName("investor")
    private MOInvestor investor;
    @SerializedName("enterprise")
    private String enterprise;
    @SerializedName("success")
    private Boolean success;

    @SerializedName("errors")
    private List<String> errors;

    public MOInvestor getInvestor() {
        return investor;
    }

    public void setInvestor(MOInvestor investor) {
        this.investor = investor;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
