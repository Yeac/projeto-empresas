package com.projeto.empresas.network.util;

import android.content.Context;

import com.projeto.empresas.models.MOAuthCredentials;
import com.projeto.empresas.models.MOInvestor;
import com.projeto.empresas.models.MOProfile;
import com.projeto.empresas.network.ApiClient;
import com.projeto.empresas.network.response.RPLoginCred;
import com.projeto.empresas.network.response.RPLoginCredError;
import com.projeto.empresas.util.EmpresasParser;
import com.projeto.empresas.util.EmpresasSharedPreferences;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ApiUtil {

    public static RPLoginCredError ApiErrorParse(Response<?> response) {
        Converter<ResponseBody, RPLoginCredError> converter =
                ApiClient.retrofit.responseBodyConverter(RPLoginCredError.class, new Annotation[0]);
        RPLoginCredError error;
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new RPLoginCredError();
        }
        return error;
    }

    public static String getApiErrorMessage(RPLoginCredError obj) {
        String send = "";
        if (obj == null) return "No message";
        if (obj.getErrors().isEmpty()) return "No message";
        for (String x : obj.getErrors()) send += x;
        return send;
    }

    public static void setAuthCredentials(Response<?> response, Context context) {
        MOAuthCredentials auth = new MOAuthCredentials();

        String access_token = response.raw().header("access-token");
        String client = response.raw().header("client");
        String uid = response.raw().header("uid");

        auth.setAccess_token(access_token);
        auth.setClient(client);
        auth.setUid(uid);

        EmpresasSharedPreferences.setAccessTokenSp(context, access_token);
        EmpresasSharedPreferences.setClientSp(context, client);
        EmpresasSharedPreferences.setUidSp(context, uid);
        EmpresasSharedPreferences.setAuthCredentialsSp(context, auth);
    }

    public static MOProfile getUserProfile(Context context) {
        return EmpresasSharedPreferences.getProfileSp(context);
    }

    public static void setUserProfile(RPLoginCred response, Context context) {
        if (response == null) return;
        MOInvestor obj = response.getInvestor();
        MOProfile profile = new MOProfile();
        profile.setId(obj.getId());
        profile.setInvestor_name(obj.getInvestorName());
        profile.setEmail(obj.getEmail());
        profile.setCity(obj.getCity());
        profile.setCountry(obj.getCountry());
        EmpresasSharedPreferences.setProfileSp(context, profile);
    }

}
