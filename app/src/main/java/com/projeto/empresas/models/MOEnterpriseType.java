package com.projeto.empresas.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MOEnterpriseType implements Serializable {

    @SerializedName("id")
    private Integer id;
    @SerializedName("enterprise_type_name")
    private String enterpriseTypeName;

    public MOEnterpriseType(Integer id, String enterpriseTypeName) {
        this.id = id;
        this.enterpriseTypeName = enterpriseTypeName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnterpriseTypeName() {
        return enterpriseTypeName;
    }

    public void setEnterpriseTypeName(String enterpriseTypeName) {
        this.enterpriseTypeName = enterpriseTypeName;
    }
}
