package com.projeto.empresas.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MOInvestor implements Serializable {

    @SerializedName("id")
    private Integer id;
    @SerializedName("investor_name")
    private String investorName;
    @SerializedName("email")
    private String email;
    @SerializedName("city")
    private String city;
    @SerializedName("country")
    private String country;
    @SerializedName("balance")
    private Integer balance;
    @SerializedName("photo")
    private Object photo;
    @SerializedName("portfolio")
    private MOPortfolio portfolio;
    @SerializedName("portfolio_value")
    private Integer portfolioValue;
    @SerializedName("first_access")
    private Boolean firstAccess;
    @SerializedName("super_angel")
    private Boolean superAngel;

    public MOInvestor(Integer id, String investorName, String email, String city, String country, Integer balance, Object photo, MOPortfolio portfolio, Integer portfolioValue, Boolean firstAccess, Boolean superAngel) {
        this.id = id;
        this.investorName = investorName;
        this.email = email;
        this.city = city;
        this.country = country;
        this.balance = balance;
        this.photo = photo;
        this.portfolio = portfolio;
        this.portfolioValue = portfolioValue;
        this.firstAccess = firstAccess;
        this.superAngel = superAngel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvestorName() {
        return investorName;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Object getPhoto() {
        return photo;
    }

    public void setPhoto(Object photo) {
        this.photo = photo;
    }

    public MOPortfolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(MOPortfolio portfolio) {
        this.portfolio = portfolio;
    }

    public Integer getPortfolioValue() {
        return portfolioValue;
    }

    public void setPortfolioValue(Integer portfolioValue) {
        this.portfolioValue = portfolioValue;
    }

    public Boolean getFirstAccess() {
        return firstAccess;
    }

    public void setFirstAccess(Boolean firstAccess) {
        this.firstAccess = firstAccess;
    }

    public Boolean getSuperAngel() {
        return superAngel;
    }

    public void setSuperAngel(Boolean superAngel) {
        this.superAngel = superAngel;
    }
}
