package com.projeto.empresas.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MOPortfolio implements Serializable {

    @SerializedName("enterprises_number")
    private Integer enterprisesNumber;
    @SerializedName("enterprises")
    private List<Object> enterprises;

    public MOPortfolio(Integer enterprisesNumber, List<Object> enterprises) {
        this.enterprisesNumber = enterprisesNumber;
        this.enterprises = enterprises;
    }

    public Integer getEnterprisesNumber() {
        return enterprisesNumber;
    }

    public void setEnterprisesNumber(Integer enterprisesNumber) {
        this.enterprisesNumber = enterprisesNumber;
    }

    public List<Object> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Object> enterprises) {
        this.enterprises = enterprises;
    }
}
